package com.blendmobi.testeV1.web.rest.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public UsuarioDTO() {
		super();
	}

	public UsuarioDTO(Long id, @NotNull @Size(max = 100) String usuario, @NotNull @Size(max = 100) String senha) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.senha = senha;
	}

	public UsuarioDTO(Long id, @NotNull @Size(max = 100) String usuario) {
		super();
		this.id = id;
		this.usuario = usuario;
	}

	public UsuarioDTO(@NotNull @Size(max = 100) String usuario, @NotNull @Size(max = 100) String senha) {
		super();
		this.usuario = usuario;
		this.senha = senha;
	}
	
	private Long id;

	@NotNull
	@Size(max = 100)
	private String usuario;

	@NotNull
	@Size(max = 100)
	private String senha;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}



}
