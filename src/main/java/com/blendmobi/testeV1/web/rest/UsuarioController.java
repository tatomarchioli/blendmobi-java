package com.blendmobi.testeV1.web.rest;

import com.blendmobi.testeV1.domain.Usuario;
import com.blendmobi.testeV1.service.UsuarioService;
import com.blendmobi.testeV1.web.rest.dto.LoginDTO;
import com.blendmobi.testeV1.web.rest.dto.UsuarioDTO;
import com.blendmobi.testeV1.web.rest.util.HeaderUtil;
import com.blendmobi.testeV1.web.rest.util.PaginationUtil;
import com.blendmobi.testeV1.web.rest.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class UsuarioController {

	private final Logger LOG = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	private UsuarioService usuarioService;

	@PostMapping("/usuarios")
	@ApiOperation(value = "Criar Usuario", authorizations = @Authorization("Authorization-Key"))
	public ResponseEntity<UsuarioDTO> criaUsuario(@Valid @RequestBody UsuarioDTO novoUsuarioDTO)
			throws URISyntaxException {
		LOG.info("REST request para salvar Usuario : {}", novoUsuarioDTO);

		Usuario usuario = convertFrom(novoUsuarioDTO);

		Usuario resultEntity = usuarioService.save(usuario);

		UsuarioDTO resultDTO = convertFrom(resultEntity);

		return ResponseEntity.created(new URI("/api/usuarios/" + resultDTO.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("USUARIO", resultDTO.getId().toString())).body(resultDTO);
	}

	@PutMapping("/usuarios")
	@ApiOperation(value = "Criar Usuario", authorizations = @Authorization("Authorization-Key"))
	public ResponseEntity<UsuarioDTO> atualizaUsuario(@Valid @RequestBody UsuarioDTO alteradoUsuarioDTO)
			throws URISyntaxException {
		LOG.info("REST request para Atualizar Usuario : {}", alteradoUsuarioDTO);

		Usuario usuario = convertFrom(alteradoUsuarioDTO);

		Optional<Usuario> result = usuarioService.update(usuario);

		Optional<UsuarioDTO> resultDTO = convertFrom(result);

		return ResponseUtil.wrapOrNotFound(resultDTO,
				HeaderUtil.createEntityUpdateAlert("USUARIO", alteradoUsuarioDTO.getId().toString()));
	}

	@GetMapping("/usuarios")
	@ApiOperation(value = "Listar Usuarios", authorizations = @Authorization("Authorization-Key"))
	public ResponseEntity<List<UsuarioDTO>> listaUsuarios(@RequestParam("page") int pageIndex,
			@RequestParam("size") int pageSize) {
		LOG.info("REST request para recuperar todos os usuarios com paginação");
		Page<Usuario> page = usuarioService.findAll(PageRequest.of(pageIndex, pageSize));

		List<UsuarioDTO> dtoList = page.getContent().stream().map(c -> new UsuarioDTO(c.getId(), c.getUsuario()))
				.collect(Collectors.toList());

		Page<UsuarioDTO> dtoPage = new PageImpl<UsuarioDTO>(dtoList);

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(dtoPage, "/api/usuarios");
		return new ResponseEntity<>(dtoPage.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/usuarios/{id}")
	@ApiOperation(value = "Recuperar Usuario", authorizations = @Authorization("Authorization-Key"))
	public ResponseEntity<UsuarioDTO> recuperaUsuario(@PathVariable Long id) {
		LOG.info("REST request que recupera Usuario de ID: {}", id);
		Optional<Usuario> result = usuarioService.findById(id);
		
		Optional<UsuarioDTO> resultDTO = convertFrom(result);
		
		return ResponseUtil.wrapOrNotFound(resultDTO);
	}

	@DeleteMapping("/usuarios/{id}")
	@ApiOperation(value = "Remover Usuario", authorizations = @Authorization("Authorization-Key"))
	public ResponseEntity<UsuarioDTO> removeUsuario(@PathVariable Long id) {
		LOG.info("REST request que deleta Usuario de ID: {}", id);
		Optional<Usuario> result = usuarioService.delete(id);
		
		Optional<UsuarioDTO> resultDTO = convertFrom(result);
		
		return ResponseUtil.wrapOrNotFound(resultDTO, HeaderUtil.createEntityDeletionAlert("USUARIO", id.toString()));
	}


	@PostMapping("/login")
	@ApiOperation("Login")
	public void login(@Valid @RequestBody LoginDTO credenciais) {
		throw new IllegalStateException("Método implementado por spring filters.");
	}

	
	// UTIL
	private UsuarioDTO convertFrom(Usuario usuario) {
		
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		usuarioDTO.setId(usuario.getId());
		usuarioDTO.setUsuario(usuario.getUsuario());
		return usuarioDTO;
	}

	private Usuario convertFrom(UsuarioDTO usuarioDTO) {
		
		Usuario usuario = new Usuario();
		usuario.setId(usuarioDTO.getId());
		usuario.setUsuario(usuarioDTO.getUsuario());
		usuario.setSenha(usuarioDTO.getSenha());
		return usuario;
	}
	
	private Optional<UsuarioDTO> convertFrom(Optional<Usuario> usuario) {
		
		if (usuario.isPresent()) {
			return Optional.of(convertFrom(usuario.get()));
		}
		
		return Optional.empty();
	}

}
