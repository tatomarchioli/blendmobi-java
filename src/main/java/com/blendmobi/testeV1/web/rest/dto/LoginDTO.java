package com.blendmobi.testeV1.web.rest.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class LoginDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public LoginDTO() {
		super();
	}

	public LoginDTO(@NotNull @Size(max = 100) String usuario, @NotNull @Size(max = 100) String senha) {
		super();
		this.usuario = usuario;
		this.senha = senha;
	}
	
	@NotNull
	@Size(max = 100)
	private String usuario;

	@NotNull
	@Size(max = 100)
	private String senha;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
