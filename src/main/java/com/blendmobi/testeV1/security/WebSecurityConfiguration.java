package com.blendmobi.testeV1.security;

import com.blendmobi.testeV1.domain.Usuario;
import com.blendmobi.testeV1.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Arrays;

import static com.blendmobi.testeV1.service.UsuarioService.encoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final UsuarioService usuarioService;

	@Autowired
	private Environment environment;

	@Autowired
	public WebSecurityConfiguration(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.csrf().disable().authorizeRequests()
				.antMatchers(HttpMethod.POST, "/api/login")
				.permitAll()
				.antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**")
				.permitAll()

				.antMatchers("/").permitAll()
				.anyRequest().authenticated()
				.and()

				// filtra requisições de login
				.addFilterBefore(new JWTLoginFilter("/api/login", authenticationManager()),
						UsernamePasswordAuthenticationFilter.class)

				// filtra outras requisições para verificar a presença do JWT no header
				.addFilterBefore(new JWTAuthenticationFilter(),
						UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		if(Arrays.stream(environment.getActiveProfiles()).anyMatch(env -> (env.equalsIgnoreCase("dev")) )){
			DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
			authProvider.setUserDetailsService(username -> {
				if(!username.equals("root")){
					throw new UsernameNotFoundException(username);
				}
                Usuario usuario = new Usuario();
                usuario.setId(1L);
                usuario.setUsuario(username);
                usuario.setSenha(encoder().encode("123"));
                return new AppUserPrincipal(usuario);
            });
			authProvider.setPasswordEncoder(encoder());
			return authProvider;
		}else {
			DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
			authProvider.setUserDetailsService(usuarioService);
			authProvider.setPasswordEncoder(encoder());
			return authProvider;
		}


	}


}