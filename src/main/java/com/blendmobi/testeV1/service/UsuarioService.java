package com.blendmobi.testeV1.service;

import com.blendmobi.testeV1.domain.Usuario;
import com.blendmobi.testeV1.repository.UsuarioRepository;
import com.blendmobi.testeV1.security.AppUserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UsuarioService implements UserDetailsService {
	
	private final Logger LOG = LoggerFactory.getLogger(UsuarioService.class);

	@Autowired
	private UsuarioRepository usuarioRepository;


	public static PasswordEncoder encoder() {
		return new BCryptPasswordEncoder(11);
	}

	private String encodePassword(String password){
		return encoder().encode(password);
	}

	public Usuario save(@Valid Usuario usuario) {
		usuario.setCriadoEm(LocalDateTime.now());
		usuario.setSenha(encodePassword(usuario.getSenha()));
		usuario = usuarioRepository.save(usuario);
		LOG.info("Usuario {} salvo com sucesso!", usuario);
		return usuario;
	}

	public Optional<Usuario> update(@Valid Usuario usuario) {
		Optional<Usuario> recuperado = usuarioRepository.findById(usuario.getId());
		if (recuperado.isPresent()) {
			LOG.info("Usuario de ID: {} encontrado: {}", recuperado.get().getId(), recuperado.get());
			if (!usuario.getUsuario().equals("root"))
				recuperado.get().setUsuario(usuario.getUsuario());
			recuperado.get().setSenha(encodePassword(usuario.getSenha()));
			Usuario atualizado = usuarioRepository.save(recuperado.get());
			LOG.info("Usuario {} atualizado com sucesso!", atualizado);
			return Optional.of(atualizado);
		}
		return Optional.empty();
	}

	public Page<Usuario> findAll(Pageable pageable) {
		LOG.info("Listando todos os usuarios com paginação!");
		return usuarioRepository.findAll(pageable);
	}

	public Optional<Usuario> findById(Long id) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if (usuario.isPresent()) {
			LOG.info("Usuario de ID: {} encontrado: {}", id, usuario.get());
			return usuario;
		}

		LOG.warn("Usuario de ID: " + id + " não encontrado!");
		return Optional.empty();
	}

	public Optional<Usuario> delete(Long id) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if (usuario.isPresent()) {
			if (usuario.get().getUsuario().equals("root")){
				LOG.warn("Não é permitido deletar o Usuario root {}!", usuario.get());
				return usuario;
			}
			LOG.info("Removendo usuario {}!", usuario.get());
			usuarioRepository.delete(usuario.get());
			return usuario;
		}

		LOG.warn("Usuario de ID: {} não encontrado!", id);
		return Optional.empty();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Usuario> user = usuarioRepository.findByUsuario(username);
		if(user.isPresent()){
			return new AppUserPrincipal(user.get());
		}
		throw new UsernameNotFoundException(username);
	}

}
