package com.blendmobi.testeV1.web.test;

import com.blendmobi.testeV1.domain.Usuario;
import com.blendmobi.testeV1.repository.ClienteRepository;
import com.blendmobi.testeV1.repository.UsuarioRepository;
import com.blendmobi.testeV1.web.rest.UsuarioController;
import com.blendmobi.testeV1.web.rest.dto.LoginDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureRestDocs
@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
@ActiveProfiles("dev")
public class UsuarioControllerTest {

	@MockBean
	UsuarioRepository repository;

	@MockBean
	ClienteRepository repositoryCliente;

	@Autowired
	MockMvc mockMvc;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setUp() {
		Usuario usuario = new Usuario("root", "123", LocalDateTime.now());
		usuario.setId(1L);

		List<Usuario> list = new ArrayList<Usuario>(1);
		list.add(usuario);

		Page<Usuario> page = new PageImpl<Usuario>(list);

		when(repository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
		when(repository.findById(1L)).thenReturn(Optional.of(usuario));
		when(repository.findAll(Mockito.any(Pageable.class))).thenReturn(page);
	}


    @Test
    public void wrongUsername() throws Exception {
        LoginDTO loginDto = new LoginDTO("roote", "123");
        mockMvc.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginDto)))
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void wrongPassword() throws Exception {
        LoginDTO loginDto = new LoginDTO("root", "1234");
        mockMvc.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginDto)))
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void login() throws Exception {
        LoginDTO loginDTO = new LoginDTO("root", "123");
        mockMvc.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginDTO)))
                .andExpect(status().isOk())
                .andExpect(header().exists("Authorization"))
                .andDo(document("{method-name}/",
                        requestFields(fieldWithPath("usuario").description("Nome de Usuario"),
                                fieldWithPath("senha").description("Senha do Usuario"))));
    }


/*

    private String token = null;
	private String getToken() throws Exception {
		if(token == null) {
			token = TokenAuthenticationService.generateToken("root");
		}
		return token;
	}


	@Test
	public void criandoUsuario() throws Exception {
		UsuarioDTO usuarioDTO = new UsuarioDTO("root", "123");
		mockMvc.perform(post("/api/usuarios")
				.header("Authorization", getToken())
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(usuarioDTO)))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.usuario", is(equalTo("root"))))
				.andDo(document("{method-name}/",
						requestFields(fieldWithPath("id").description("Id do Usuario").ignored(),
								fieldWithPath("usuario").description("Nome de Usuario"),
								fieldWithPath("senha").description("Senha do Usuario"))));
	}

	@Test
	public void atualizaUsuario() throws Exception {

		UsuarioDTO usuarioDTO = new UsuarioDTO("usuario_slave", "1234");
		usuarioDTO.setId(1L);

		mockMvc.perform(put("/api/usuarios")
				.header("Authorization", getToken())
				.contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(usuarioDTO)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.usuario", is(equalTo("usuario_slave"))))
				.andDo(document("{method-name}/",
						requestFields(fieldWithPath("id").description("Id do Usuario").ignored(),
								fieldWithPath("usuario").description("Nome de Usuario"),
								fieldWithPath("senha").description("Senha do Usuario"))));
	}

	@Test
	public void removeUsuario() throws Exception {
		this.mockMvc.perform(delete("/api/usuarios/{id}", 1L)
				.header("Authorization", getToken())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.usuario", is(equalTo("root"))))
				.andDo(document("{method-name}/",
						responseFields(fieldWithPath("id").description("Id do Usuario"),
								fieldWithPath("usuario").description("Nome de Usuario"),
								fieldWithPath("senha").description("Senha do Usuario")),
						pathParameters(parameterWithName("id").description("Id do Usuario"))));
	}

	@Test
	public void findByIdUsuario() throws Exception {
		this.mockMvc.perform(get("/api/usuarios/{id}", 1L)
				.header("Authorization", getToken())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andDo(document("{method-name}/",
						responseFields(fieldWithPath("id").description("Id do Usuario"),
								fieldWithPath("usuario").description("Nome de Usuario"),
								fieldWithPath("senha").description("Senha do Usuario")),
						pathParameters(parameterWithName("id").description("Id do Usuario"))));
	}

	@Test
	public void listUsuario() throws Exception {
		this.mockMvc.perform(get("/api/usuarios?page=0&size=10")
				.header("Authorization", getToken())
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andDo(document("{method-name}/",
						responseFields(fieldWithPath("[].id").description("Id do Usuario"),
								fieldWithPath("[].senha").description("Senha do Usuario"),
								fieldWithPath("[].usuario").description("Nome de Usuario"))));
	}
	*/

}
