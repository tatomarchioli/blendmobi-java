# Desafio Java

###  Contexto

Seja bem-vindo ao nosso desafio técnico para entusistas da plataforma Java! Testaremos seus skills ao manusear um projeto que 
usa como base uma stack constituída por uma aplicação Spring Boot sobre Java 8 com base de dados PostgreSQL e H2, com dependências geridas com Maven e com possibilidade de rodar tanto localmente como em cloud na Heroku. 

### Desafio

A aplicação em sua versão 1 expõe uma API através de alguns controllers com acesso anônimo sem restrições - confira ao rodar o Swagger-UI do tutorial abaixo - e apesar de ser muito legal poder acessar toda a API dessa forma, esses serviços estariam totalmente expostos a ataques diversos. Para corrigir tal falha pedimos que seja implementa segurança para acesso à API, seguindo algumas premissas:

1. Usar [Spring Security](https://spring.io/projects/spring-security)
2. Usar [JWT](https://jwt.io/)
3. Todos endpoints do controller ClienteController devem ser seguros ao acesso
4. Todos endpoints do controller HealthController continuarão com acesso sem restrição
5. Os testes devem ser ajustados e devem continuar a passar corretamente

### Nice to Have

Os itens contidos nessa seção não são obrigatórios, porém é sua chance de nos surpreender :).

6. Criar uma nova tabela de bancos de dados usando as migrations do [liquibase](https://www.liquibase.org/) para guardar o login e senha dos usuários do sistema. Lembrando que o projeto já faz uso da ferramenta, veja: [liquibase-changeLog.xml](src/main/resources/db/changelog/liquibase-changeLog.xml)

7. Fazer a aplicação usar o login e senha guardados na tabela criada como credenciais para obter o token de autenticação da API. 

### Formato de Entrega

Você deve fazer um fork deste repositório e enviar um Pull Request com sua implementação. Se sinta a vontade para comentar sua solução mesmo que não consiga terminar todos os itens. Isso é tão importante quanto o código gerado.

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------

# Teste V1

Este é projeto experimental de uma API Rest que cuida do ciclo de vida de uma entidade Cliente.

## Começando

Segue então o passo a passo de como rodar o projeto em seu ambiente de desenvolvimento.

### Pré-requisitos

1. Git 2.7.4 ou superior
2. Java JDK 1.8 ou superior
3. Maven 3.5.3 ou superior
4. Docker 18.06.1-ce ou superior **(Opcional)**
5. Heroku CLI 7.18.10 ou superior **(Opcional)**

Os itens 4 e 5 serão usados para deploy na nuvem, caso não seja o objetivo, não se faz necessário a instalação dos mesmos.

### Obtendo os fontes

#### Fazer o checkout do projeto em: https://bitbucket.org/blendmobi/java/

```
$ git clone https://bitbucket.org/blendmobi/java/
```

#### Entrar na pasta do projeto

```
$ cd testeV1
```

#### Limpar, compilar, testar e empacotar

Neste passo todos as dependências do projeto serão baixadas, então o projeto será compilado, os testes automatizados irão rodar e, em caso de sucesso, o projeto será empacotado e ficará pronto para ser executado localmente.

```
mvn clean compile test package
```

#### Rodando!

```
mvn spring-boot:run
```

Será exibido a mensagem final:

```
2018-12-03 20:46:01.127  INFO 27544 --- [           main] c.blendmobi.testeV1.TesteV1Application    : Started TesteV1Application in 6.724 seconds (JVM running for 11.065)
```

## Experimentando a API

Junto a aplicação também fica disponível o **swagger**, que deve estar acessível através de:
```
http://localhost:8080/swagger-ui.html
```
Abra seu navegador e aponte para essa url. Através da swagger-ui será possivel conhecer todos os endpoints e também a estrutura deste, assim como testar as chamadas e seus resultados.

## Documentação da API

Usando Spring Rest Docs foi gerado um html com a documentação de utilização da api, tal arquivo se encontra na raíz do projeto com nomde de [api-guide.html](api-guide.html) , abra-o em seu ambiente de desenvolvimento usando seu navegador preferido.

## Colocando em Produção

O projeto por ter a estrutura de **Spring-Boot** pode ser implantado na maioria dos provedores do mercado, tais como: AWS, Heroku, OpenShift, Cloud Foundry entre outros. Mas pelo natureza experimental foi decidido disponibilizar o passo a passo de como implementar no Heroku em detalhes e também como uma imagem docker para que possa ser usada em outros provedores que suportem.

### Heroku

Existem algumas formas de colocar nosso projeto para rodar na heroku, e uma mais simples é utilizando o próprio git para tal.

#### Estando dentro da pasta root do projeto e tendo o heroku cli instalado podemos executar:
```
heroku create -a <SEU_PREFIXO>-teste-v1
```
Onde *<SEU_PREFIXO>* deve ser substituído por algum valor de sua preferência.
Esse comando cria um novo App em sua conta heroku e também associa um remote chamado *heroku* a seus repositórios remotos do projeto.

#### Precisamos agora criar uma instância de banco de dados e associar a essa aplicação dentro da heroku, fazendo:
```
$ heroku addons:create heroku-postgresql
Creating heroku-postgresql on ⬢ <SEU_PREFIXO>-teste-v1... free
Database has been created and is available
 ! This database is empty. If upgrading, you can transfer
 ! data from another database with pg:copy
Created postgresql-amorphous-90907 as DATABASE_URL
```
Após a execução com sucesso do comando podemos consultar a url onde o banco de dados está trabalhando:
```
$ heroku config
=== <SEU_PREFIXO>-teste-v1 Config Vars
DATABASE_URL: postgres://pmupewqnwvgjkq:6a1452d3014c56d938e3a5bf44d9f0f300d1cc805347e5e2f2d3440755ee240d@ec2-107-21-125-209.compute-1.amazonaws.com:5432/dnn93f7mrc72f
```
Obs: Quando o heroku provisiona uma base para a aplicação, automaticamente é registrado uma variável dentro do ambiente da aplicação: DATABASE_URL, ver uso em [application-prd.yml](src/main/resources/application-prd.yml)

#### Devemos configurar agora um variável de ambiente dentro da aplicação para que possa ser usada uma base PostgreSQL:
Spring Profile Active
```
$ heroku config:set SPRING_PROFILES_ACTIVE=prd
Setting SPRING_PROFILES_ACTIVE and restarting ⬢ <SEU_PREFIXO>-teste-v1... done, v5
SPRING_PROFILES_ACTIVE: prd 
```
#### Agora devemos enviar o código da nossa aplicação diretamente para o remote heroku configurado anteriormente:
```
$ git push heroku master
```
Após alguns minutos a aplicação já está pronta para uso através da **URL: https://<SEU_PREFIXO>-teste-v1.herokuapp.com/**

## Construído com

* [Java](https://www.oracle.com/java/) - Java Platform
* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring Boot](https://spring.io/projects/spring-boot) - Framework and Tools
* [Eclipse](https://www.eclipse.org) - IDE
* [Linux Mint](https://linuxmint.com/) - OS

## Autor
* **BlendMobi** - **Samarone Lopes** - [samarone.lopes@blendmobi.com)
